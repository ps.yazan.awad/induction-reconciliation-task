package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.comparison.ResultsHolder;
import com.progressoft.jip9.finance.comparison.Comparison;
import com.progressoft.jip9.finance.comparison.TransactionComparison;
import com.progressoft.jip9.finance.exceptions.CsvFileWriterException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CsvFileWriterTest {
    private ResultsFileWriter csvWriter;

    @BeforeEach
    public void setup() {
        csvWriter = new CsvFileWriter();
    }

    @Test
    public void canCreate() {
        new CsvFileWriter();
    }

    @Test
    public void givenNullResultSet_whenWrite_ThenFail() {
        ResultsHolder resultSet = null;
        CsvFileWriterException exception = assertThrows(CsvFileWriterException.class, () -> csvWriter.write(resultSet));
        assertEquals("Null Result set", exception.getMessage());
    }

    @Test
    public void givenResultSetButNullLists_whenWrite_ThenFail() {
        ResultsHolder resultSet = new ResultsHolder(null, null, null);
        CsvFileWriterException exception = assertThrows(CsvFileWriterException.class, () -> csvWriter.write(resultSet));
        assertEquals("Null lists in Result set", exception.getMessage());
    }

    @Test
    void givenResultSet_whenWrite_ThenThreeCsvFilesAreWritten() throws IOException {
        Path csvPath = Paths.get(".." + "/sample-files/input-files/bank-transactions.csv");
        Path jsonPath = Paths.get(".." + "/sample-files/input-files/online-banking-transactions.json");
        TransactionsReader firstReader = new CSVfileReader();
        TransactionsReader secondReader = new JsonFileReader();
        Comparison comparison = new TransactionComparison();
        List<Transaction> first = new LinkedList<>();
        List<Transaction> second = new LinkedList<>();
        firstReader.read(csvPath, (record) -> first.add((Transaction) record));
        secondReader.read(jsonPath, (record) -> second.add((Transaction) record));
        csvWriter.write(comparison.Compare(first, second));

        List<String> matchingLinesExpected = prepareMatchingLines();
        List<String> mismatchingLinesExpected = prepareMismatchingLines();
        List<String> missingLinesExpected = prepareMissingLines();

        boolean matching =isCorrectFile(Paths.get("../reconciliation-results/Matching Transactions.csv"), matchingLinesExpected);
        boolean mismatching = isCorrectFile(Paths.get("../reconciliation-results/Mismatching Transactions.csv"), mismatchingLinesExpected);
        boolean missing = isCorrectFile(Paths.get("../reconciliation-results/Missing Transactions.csv"), missingLinesExpected);
        Assertions.assertTrue(matching,"matching file is incorrect");
        Assertions.assertTrue(mismatching,"mismatching file is incorrect");
        Assertions.assertTrue(missing,"missing file is incorrect");
    }

    boolean isCorrectFile(Path path, List<String> expectedLines) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            for (String lineExpected : expectedLines) {
                line = reader.readLine();
                if(!lineExpected.equals(line))
                    return false;
            }
        }
        return true;
    }

    private List<String> prepareMissingLines() {
        List<String> missingLinesExpected = new LinkedList<>();
        missingLinesExpected.add("found in file,transaction id,amount,currecny code,value date");
        missingLinesExpected.add("SOURCE,TR-47884222204,1200.000,JOD,2020-01-31");
        missingLinesExpected.add("TARGET,TR-47884222217,12000.000,JOD,2020-02-14");
        missingLinesExpected.add("TARGET,TR-47884222245,420.00,USD,2020-01-12");
        return missingLinesExpected;
    }

    private List<String> prepareMismatchingLines() {
        List<String> mismatchingLinesExpected = new LinkedList<>();
        mismatchingLinesExpected.add("found in file,transaction id,amount,currecny code,value date");
        mismatchingLinesExpected.add("SOURCE,TR-47884222202,30.000,JOD,2020-01-22");
        mismatchingLinesExpected.add("SOURCE,TR-47884222205,60.000,JOD,2020-02-03");
        mismatchingLinesExpected.add("TARGET,TR-47884222205,60.000,JOD,2020-02-03");
        mismatchingLinesExpected.add("TARGET,TR-47884222202,30.000,JOD,2020-01-22");
        return mismatchingLinesExpected;
    }

    private List<String> prepareMatchingLines() {
        List<String> matchingLinesExpected = new LinkedList<>();
        matchingLinesExpected.add("transaction id,amount,currecny code,value date");
        matchingLinesExpected.add("TR-47884222201,140.00,USD,2020-01-20");
        matchingLinesExpected.add("TR-47884222203,5000.000,JOD,2020-01-25");
        matchingLinesExpected.add("TR-47884222206,500.00,USD,2020-02-10");
        return matchingLinesExpected;
    }


}
