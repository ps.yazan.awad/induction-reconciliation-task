package com.progressoft.jip9.finance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CSVfileReaderTest {
    private TransactionsReader csvReader;

    @BeforeEach
    public void setup() {
        csvReader = new CSVfileReader();
    }

    @Test
    public void canCreate() {
        new CSVfileReader();
    }

    @Test
    public void givenNotValidPath_whenRead_thenFail() {
        Path path = Paths.get("." + new Random().nextInt());
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> csvReader.read(path));
        Assertions.assertEquals("path does not exists", thrown.getMessage());
        Path path2 =null;
        NullPointerException thrown2 = Assertions.assertThrows(NullPointerException.class,
                () -> csvReader.read(path2));
        Assertions.assertEquals("null path", thrown2.getMessage());
        Path path3 = Paths.get("." );
        IllegalArgumentException thrown3 = Assertions.assertThrows(IllegalArgumentException.class,
                () -> csvReader.read(path3));
        Assertions.assertEquals("path is not a file", thrown3.getMessage());
    }
    @Test void givenValidPath_whenRead_thenRecordsReturned(){
        Path path = Paths.get(".."+ "/sample-files/input-files/bank-transactions.csv");
        List<Transaction> records = new ArrayList<>();
        csvReader.read(path, (record)->records.add((Transaction) record));
        Assertions.assertFalse(records.isEmpty(),"No records are returned !");
    }
    @Test void givenValidPath_whenRead_thenRecordsReturnedCorrectly(){
        Path path = Paths.get(".."+ "/sample-files/input-files/bank-transactions.csv");
        List<Transaction> records = new ArrayList<>();
        csvReader.read(path, (record)->records.add((Transaction) record));
        Assertions.assertFalse(records.isEmpty(),"No records are returned !");
        //System.out.println(records);
        Transaction testTransaction = new TransactionBuilder()
                .setReference(47884222201L)
                .setDescription("online transfer")
                .setAmount(new BigDecimal("140"))
                .setCurrency("USD")
                .setDate(LocalDate.of(2020,1,20))
                .setPurpose("donation")
                .setType(TransactionType.DEBIT)
                .createTransaction();
        Assertions.assertEquals(testTransaction,records.get(0),"First record returned is not the same as Expected");

        Transaction testTransaction2 = new TransactionBuilder()
                .setReference(47884222202L)
                .setDescription("atm withdrwal")
                .setAmount(new BigDecimal("20.0000"))
                .setCurrency("JOD")
                .setDate(LocalDate.of(2020,1,22))
                .setPurpose("")
                .setType(TransactionType.DEBIT)
                .createTransaction();
        Assertions.assertEquals(testTransaction2,records.get(1),"Second record returned is not the same as Expected");
    }
}
