package com.progressoft.jip9.finance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

public class JsonFileReaderTest {
    private TransactionsReader jsonReader;

    @BeforeEach
    public void setup() {
        jsonReader = new JsonFileReader();
    }

    @Test
    public void canCreate() {
        new JsonFileReader();
    }

    @Test
    public void givenNotValidPath_whenRead_thenFail() {
        Path path = Paths.get("." + new Random().nextInt());
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> jsonReader.read(path));
        Assertions.assertEquals("path does not exists", thrown.getMessage());
        Path path2 = null;
        NullPointerException thrown2 = Assertions.assertThrows(NullPointerException.class,
                () -> jsonReader.read(path2));
        Assertions.assertEquals("null path", thrown2.getMessage());
        Path path3 = Paths.get(".");
        IllegalArgumentException thrown3 = Assertions.assertThrows(IllegalArgumentException.class,
                () -> jsonReader.read(path3));
        Assertions.assertEquals("path is not a file", thrown3.getMessage());
    }

    @Test
    void givenValidPath_whenRead_thenRecordsReturned() {
        Path path = Paths.get(".." + "/sample-files/input-files/online-banking-transactions.json");
        List<Transaction> records = new ArrayList<>();
        jsonReader.read(path, (r) -> records.add((Transaction) r));
        Assertions.assertFalse(records.isEmpty(), "No records are returned !");
        //System.out.println(records);
    }

    @Test
    void givenValidPath_whenRead_thenRecordsReturnedCorrectly() {
        Path path = Paths.get(".." + "/sample-files/input-files/online-banking-transactions.json");
        List<Transaction> records = new ArrayList<>();
        jsonReader.read(path, (r) -> records.add((Transaction) r));
        Assertions.assertFalse(records.isEmpty(), "No records are returned !");

        Transaction testRecord = new TransactionBuilder()
                .setAmount(new BigDecimal("140"))
                .setCurrency("USD")
                .setDate(LocalDate.of(2020, 1, 20))
                .setPurpose("donation")
                .setReference(47884222201L)
                .createTransaction();
        Assertions.assertEquals(testRecord, records.get(0), "First record returned is not the same as Expected");

        Transaction testRecord2 = new TransactionBuilder()
                .setReference(47884222205L)
                .setCurrency("JOD")
                .setAmount(new BigDecimal("60"))
                .setDate(LocalDate.of(2020, 2, 3))
                .setPurpose("")
                .createTransaction();

        Assertions.assertEquals(testRecord2, records.get(1), "Second record returned is not the same as Expected");
    }
}
