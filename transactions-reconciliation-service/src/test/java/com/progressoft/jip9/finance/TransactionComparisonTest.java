package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.comparison.CompareResultRecord;
import com.progressoft.jip9.finance.comparison.ResultsHolder;
import com.progressoft.jip9.finance.comparison.Comparison;
import com.progressoft.jip9.finance.comparison.TransactionComparison;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class TransactionComparisonTest {
    private Comparison transactionComparison;

    @BeforeEach
    public void setup() {
        transactionComparison = new TransactionComparison();
    }

    @Test
    public void canCreate() {
        new TransactionComparison();
    }

    @Test
    public void givenTwoListOfTransactions_whenCompare_thenResultsReturned() {

        List<List<Transaction>> lists = prepareRecords();
        ResultsHolder resultSet = transactionComparison.Compare(lists.get(0), lists.get(1));
        //TODO assert more than records count, you should assert the content
        Assertions.assertEquals(1, resultSet.getMatchingTransactions().size(), "Matching Results are not as expected");
        Assertions.assertEquals(2, resultSet.getMismatchingTransactions().size(), "Mismatching Results are not as expected");
        Assertions.assertEquals(1, resultSet.getMissingTransactions().size(), "Missing Results are not as expected");
        List<Transaction> expectedMatchingList = new LinkedList<>();
        expectedMatchingList.add(lists.get(0).get(0));

        CompareResultRecord mismatch = new CompareResultRecord(lists.get(1).get(1), "SOURCE");
        CompareResultRecord mismatch2 = new CompareResultRecord(lists.get(1).get(1), "TARGET");
        List<CompareResultRecord> expectedMismatchingList = new LinkedList<>();
        expectedMismatchingList.add(mismatch);
        expectedMismatchingList.add(mismatch2);
        Assertions.assertEquals(expectedMatchingList, resultSet.getMatchingTransactions(), "Matching Results are not as expected");
        Assertions.assertEquals(expectedMismatchingList.toString(), resultSet.getMismatchingTransactions().toString(), "Mismatching Results are not as expected");
    }

    private List<List<Transaction>> prepareRecords() {
        Transaction testTransaction1 = new TransactionBuilder()
                .setReference(47884222201L)
                .setDescription("online transfer")
                .setAmount(new BigDecimal("140"))
                .setCurrency("USD")
                .setDate(LocalDate.of(2020, 1, 20))
                .setPurpose("donation")
                .setType(TransactionType.DEBIT)
                .createTransaction();
        Transaction testTransaction2 = new TransactionBuilder()
                .setReference(47884222202L)
                .setDescription("atm withdrwal")
                .setAmount(new BigDecimal("20.0000"))
                .setCurrency("JOD")
                .setDate(LocalDate.of(2020, 1, 22))
                .setPurpose("")
                .setType(TransactionType.DEBIT)
                .createTransaction();
        Transaction testTransaction3 = new TransactionBuilder()
                .setReference(47884222202L)
                .setDescription("atm withdrwal")
                .setAmount(new BigDecimal("30.0000"))
                .setCurrency("JOD")
                .setDate(LocalDate.of(2020, 1, 22))
                .setPurpose("")
                .setType(TransactionType.DEBIT)
                .createTransaction();
        Transaction testTransaction4 = new TransactionBuilder()
                .setReference(47884222203L)
                .setDescription("counter withdrawal")
                .setAmount(new BigDecimal("5000"))
                .setCurrency("JOD")
                .setDate(LocalDate.of(2020, 1, 25))
                .setPurpose("donation")
                .setType(TransactionType.DEBIT)
                .createTransaction();
        List<Transaction> firstList = new LinkedList<>();
        firstList.add(testTransaction1);
        firstList.add(testTransaction2);
        List<Transaction> secondList = new LinkedList<>();
        secondList.add(testTransaction1);
        secondList.add(testTransaction3);
        secondList.add(testTransaction4);
        List<List<Transaction>> lists = new LinkedList<>();
        lists.add(firstList);
        lists.add(secondList);
        return lists;
    }
}
