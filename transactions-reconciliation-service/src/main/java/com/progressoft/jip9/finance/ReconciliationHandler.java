package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.comparison.Comparison;
import com.progressoft.jip9.finance.comparison.ResultsHolder;
import com.progressoft.jip9.finance.comparison.TransactionComparison;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public class ReconciliationHandler {

    public ResultsHolder handle(TransactionsReader sourceReader, TransactionsReader targetReader, Path sourcePath, Path targetPath){
        Comparison comparison = new TransactionComparison();
        List<Transaction> sourceList = new LinkedList<>();
        List<Transaction> targetList = new LinkedList<>();
        sourceReader.read(sourcePath, (record) -> sourceList.add((Transaction) record));
        targetReader.read(targetPath,(record) -> targetList.add((Transaction) record));
        ResultsHolder resultsHolder = comparison.Compare(sourceList, targetList);
        return resultsHolder;
    }
}
