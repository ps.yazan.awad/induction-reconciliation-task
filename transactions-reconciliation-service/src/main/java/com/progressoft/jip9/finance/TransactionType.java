package com.progressoft.jip9.finance;

public enum TransactionType {
    CREDIT,DEBIT
}
