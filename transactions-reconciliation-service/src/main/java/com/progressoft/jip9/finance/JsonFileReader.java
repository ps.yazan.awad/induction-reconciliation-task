package com.progressoft.jip9.finance;


import com.progressoft.jip9.finance.exceptions.JsonFileReaderException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;


public class JsonFileReader implements TransactionsReader {
    @Override
    public void read(Path path, Consumer<Transaction> consumer) {
        validatePath(path);
        JSONParser jsonParser = new JSONParser();
        try {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(path.toFile()));
            for (Object jsonObject : jsonArray) {
                Transaction record = getRecordJson((JSONObject) jsonObject);
                consumer.accept(record);
            }
        } catch (IOException e) {
            throw new JsonFileReaderException(e);
        } catch (ParseException e) {
            throw new JsonFileReaderException(e);
        }
    }

    private Transaction getRecordJson(JSONObject jsonObject) {
        String dateString = String.valueOf(jsonObject.get("date"));
        long reference = Long.parseLong(((String) jsonObject.get("reference")).split("-")[1]);
        //TODO you could use LocalDate.parse("", formatter)
        LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        BigDecimal amount = new BigDecimal((String) jsonObject.get("amount"));
        String code = (String) jsonObject.get("currencyCode");
        String purpose = (String) jsonObject.get("purpose");
        return extractTransaction(reference, date, amount, code, purpose);
    }

    private Transaction extractTransaction(long reference, LocalDate date, BigDecimal amount, String code, String purpose) {
        return new TransactionBuilder()
                .setAmount(amount)
                .setCurrency(code)
                .setDate(date)
                .setPurpose(purpose)
                .setReference(reference)
                .createTransaction();
    }
}
