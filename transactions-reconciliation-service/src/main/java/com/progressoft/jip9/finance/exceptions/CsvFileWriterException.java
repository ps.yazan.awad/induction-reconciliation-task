package com.progressoft.jip9.finance.exceptions;

public class CsvFileWriterException extends RuntimeException {
    public CsvFileWriterException(String message) {
        super(message);
    }

    public CsvFileWriterException(String message, Exception e) {
        super(message, e);
    }
}
