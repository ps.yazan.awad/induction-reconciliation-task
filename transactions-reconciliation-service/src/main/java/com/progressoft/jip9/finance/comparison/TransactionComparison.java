package com.progressoft.jip9.finance.comparison;

import com.progressoft.jip9.finance.Transaction;

import java.util.*;

public class TransactionComparison implements Comparison{
    public ResultsHolder Compare(List<Transaction> first, List<Transaction> second) {
        //TODO rename to ResultHolder
        ComponentsOfComparison components = new ComponentsOfComparison(convertToMap(first),convertToMap(second));
        firstFileCompare(first, components);
        secondFileCompare(second, components);
        return new ResultsHolder(components.matchingTransactions, components.mismatchingTransactions, components.missingTransactions);
    }

    private void secondFileCompare(List<Transaction> second, ComponentsOfComparison components) {
        for (Transaction transaction : second) {
            if (components.firstMap.containsKey(String.valueOf(transaction.getReference()))) {
                Transaction firstTransaction = components.firstMap.get(String.valueOf(transaction.getReference()));
                if (!isTransactionsEquals(firstTransaction, transaction)) {
                    components.mismatchingTransactions.add(new CompareResultRecord(transaction, "TARGET"));
                }
            } else {
                components.missingTransactions.add(new CompareResultRecord(transaction, "TARGET"));
            }
        }
    }

    private void firstFileCompare(List<Transaction> first, ComponentsOfComparison components) {
        for (Transaction transaction : first) {
            if (components.secondMap.containsKey(String.valueOf(transaction.getReference()))) {
                Transaction secondTransaction = components.secondMap.get(String.valueOf(transaction.getReference()));
                if (isTransactionsEquals(transaction, secondTransaction)) {
                    components.matchingTransactions.add(secondTransaction);
                } else {
                    components.mismatchingTransactions.add(new CompareResultRecord(secondTransaction, "SOURCE"));
                }
            } else {
                components.missingTransactions.add(new CompareResultRecord(transaction, "SOURCE"));
            }
        }
    }

    private boolean isTransactionsEquals(Transaction transaction, Transaction secondTransaction) {
        return isDateEquals(transaction, secondTransaction) && isCurrencyEquals(transaction, secondTransaction) && isAmountEquals(transaction, secondTransaction);
    }

    private boolean isAmountEquals(Transaction transaction, Transaction secondTransaction) {
        return transaction.getAmount().compareTo(secondTransaction.getAmount()) == 0;
    }

    private boolean isCurrencyEquals(Transaction transaction, Transaction secondTransaction) {
        return transaction.getCurrency().equals(secondTransaction.getCurrency());
    }

    private boolean isDateEquals(Transaction transaction, Transaction secondTransaction) {
        return transaction.getDate().equals(secondTransaction.getDate());
    }

    private LinkedHashMap<String, Transaction> convertToMap(List<Transaction> list) {
        LinkedHashMap<String, Transaction> map = new LinkedHashMap<>();
        for (Transaction transaction : list) {
            map.put(String.valueOf(transaction.getReference()), transaction);
        }
        return map;
    }
}

class ComponentsOfComparison {
    List<CompareResultRecord> missingTransactions;
    List<CompareResultRecord> mismatchingTransactions;
    List<Transaction> matchingTransactions;
    HashMap<String, Transaction> firstMap;
    HashMap<String, Transaction> secondMap;

    public ComponentsOfComparison(HashMap<String, Transaction> firstMap, HashMap<String, Transaction> secondMap) {
        this.firstMap = firstMap;
        this.secondMap = secondMap;
        missingTransactions = new LinkedList<>();
        mismatchingTransactions = new LinkedList<>();
        matchingTransactions = new LinkedList<>();
    }
}