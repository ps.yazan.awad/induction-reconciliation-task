package com.progressoft.jip9.finance.comparison;

import com.progressoft.jip9.finance.Transaction;

import java.util.Collections;
import java.util.List;

public class ResultsHolder {
    private List<Transaction> matchingTransactions;
    private List<CompareResultRecord> mismatchingTransactions;
    private List<CompareResultRecord> missingTransactions;

    public ResultsHolder(List<Transaction> matchingTransactions, List<CompareResultRecord> mismatchingTransactions, List<CompareResultRecord> missingTransactions) {
        this.matchingTransactions = matchingTransactions;
        this.mismatchingTransactions = mismatchingTransactions;
        this.missingTransactions = missingTransactions;
    }

    public List<Transaction> getMatchingTransactions() {
        return Collections.unmodifiableList(matchingTransactions);
    }

    //TODO no need for setters

    //TODO return a readonly copy
    public List<CompareResultRecord> getMismatchingTransactions() {
        return Collections.unmodifiableList(mismatchingTransactions);
    }


    public List<CompareResultRecord> getMissingTransactions() {
        return Collections.unmodifiableList(missingTransactions);
    }

}
