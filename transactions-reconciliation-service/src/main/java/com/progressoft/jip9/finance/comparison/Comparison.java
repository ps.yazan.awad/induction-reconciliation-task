package com.progressoft.jip9.finance.comparison;

import com.progressoft.jip9.finance.Transaction;

import java.util.List;

public interface Comparison {
    public ResultsHolder Compare(List<Transaction> first, List<Transaction> second);
}
