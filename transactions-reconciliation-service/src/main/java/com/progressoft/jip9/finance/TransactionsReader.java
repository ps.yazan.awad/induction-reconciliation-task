package com.progressoft.jip9.finance;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public interface TransactionsReader {

    void read(Path path, Consumer<Transaction> consumer);

    default List<Transaction> read(Path path) {
        List<Transaction> records = new LinkedList<>();
        read(path, (r) -> records.add(r));
        return records;
    }

    default void validatePath(Path path) {
        if (path == null)
            throw new NullPointerException("null path");
        if (Files.notExists(path))
            throw new IllegalArgumentException("path does not exists");
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("path is not a file");
    }
}
