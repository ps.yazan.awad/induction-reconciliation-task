package com.progressoft.jip9.finance.comparison;

import com.progressoft.jip9.finance.Transaction;

//TODO rename to MissingRecord, it would be better
public class CompareResultRecord {
    private Transaction transaction;
    private String sourceFile;

    public CompareResultRecord(Transaction transaction, String sourceFile) {
        this.transaction = transaction;
        this.sourceFile = sourceFile;
    }

    public Transaction getTransaction() {
        return transaction;
    }
//TODO no need for setters
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    @Override
    public String toString() {
        return "CompareResultRecord{" +
                "transaction=" + transaction +
                ", sourceFile='" + sourceFile + '\'' +
                '}';
    }
}
