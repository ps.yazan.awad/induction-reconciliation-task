package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.comparison.ResultsHolder;

public interface ResultsFileWriter {
    public void write(ResultsHolder resultSet);
}
