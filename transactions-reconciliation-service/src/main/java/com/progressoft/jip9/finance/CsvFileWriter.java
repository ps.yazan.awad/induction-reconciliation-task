package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.comparison.CompareResultRecord;
import com.progressoft.jip9.finance.comparison.ResultsHolder;
import com.progressoft.jip9.finance.exceptions.CsvFileWriterException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class CsvFileWriter implements ResultsFileWriter {
    public CsvFileWriter() {
        List<Path> pathList = getPaths();
        createDirectoryIfNotExists();
        createOutputFiles(pathList);
    }

    public void write(ResultsHolder resultSet) {
        failIfNull(resultSet);
        writeToMatchingFile(resultSet);
        writeToMismatchingFiles(resultSet.getMismatchingTransactions(), "Mismatching Transactions.csv");
        writeToMismatchingFiles(resultSet.getMissingTransactions(), "Missing Transactions.csv");
    }

    private void createOutputFiles(List<Path> pathList) {
        for (Path path : pathList) {
            if (!Files.exists(path)) {
                File file = new File(path.toAbsolutePath().toString());
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    //TODO swallowing exception when not passing the cause
                    throw new CsvFileWriterException("cannot create new files", e);
                }
            }
        }
    }

    private void createDirectoryIfNotExists() {
        String directoryName = "../reconciliation-results";
        File directory = new File(directoryName);
        if (!directory.exists())
            directory.mkdir();
    }

    private List<Path> getPaths() {
        List<Path> pathList = new LinkedList<>();
        //TODO those failed at my machine
        pathList.add(Paths.get("../reconciliation-results/Matching Transactions.csv"));
        pathList.add(Paths.get("../reconciliation-results/Mismatching Transactions.csv"));
        pathList.add(Paths.get("../reconciliation-results/Missing Transactions.csv"));
        return pathList;
    }


    private void writeToMismatchingFiles(List<CompareResultRecord> resultRecords, String fileName) {
        try (PrintWriter writer = new PrintWriter(new File("../reconciliation-results/" + fileName))) {
            writer.write("found in file,transaction id,amount,currecny code,value date\n");
            for (CompareResultRecord resultRecord : resultRecords) {
                StringBuilder line = new StringBuilder();
                line.append(resultRecord.getSourceFile());
                line.append(',');
                line.append("TR-" + resultRecord.getTransaction().getReference());
                line.append(',');
                line.append(resultRecord.getTransaction().getAmount());
                line.append(',');
                line.append(resultRecord.getTransaction().getCurrency());
                line.append(',');
                line.append(resultRecord.getTransaction().getDate() + "\n");
                writer.write(line.toString());
            }
        } catch (FileNotFoundException e) {
            throw new CsvFileWriterException("Failed to Write");
        }
    }

    private void writeToMatchingFile(ResultsHolder resultSet) {
        try (PrintWriter writer = new PrintWriter(new File("../reconciliation-results/Matching Transactions.csv"))) {
            writer.write("transaction id,amount,currecny code,value date\n");
            for (Transaction transaction : resultSet.getMatchingTransactions()) {
                StringBuilder line = new StringBuilder();
                line.append("TR-" + transaction.getReference());
                line.append(',');
                line.append(transaction.getAmount());
                line.append(',');
                line.append(transaction.getCurrency());
                line.append(',');
                line.append(transaction.getDate() + "\n");
                writer.write(line.toString());
            }
        } catch (FileNotFoundException e) {
            //TODO swallowing
            throw new CsvFileWriterException("Failed to Write", e);
        }
    }

    private void failIfNull(ResultsHolder resultSet) {
        if (resultSet == null)
            throw new CsvFileWriterException("Null Result set");
        if (oneOfListsIsNull(resultSet))
            throw new CsvFileWriterException("Null lists in Result set");
    }

    private boolean oneOfListsIsNull(ResultsHolder resultSet) {
        try{
            resultSet.getMatchingTransactions();
            resultSet.getMismatchingTransactions();
            resultSet.getMissingTransactions();
        }catch (NullPointerException e){
            return true;
        }
        return false;
    }
}
