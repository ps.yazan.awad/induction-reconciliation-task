package com.progressoft.jip9.finance;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Transaction {
    private long reference;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String purpose;
    private LocalDate date;
    private TransactionType type;

    public Transaction(long reference, String description, BigDecimal amount, String currency, String purpose, LocalDate date, TransactionType type) {
        this.reference = reference;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.purpose = purpose;
        this.date = date;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return reference == that.reference &&
                Objects.equals(description, that.description) &&
                (amount.compareTo(that.amount)==0) &&
                currency.equals(that.currency) &&
                Objects.equals(purpose, that.purpose) &&
                date.equals(that.date) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference, description, amount, currency, purpose, date, type);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "reference=" + reference +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", purpose='" + purpose + '\'' +
                ", date=" + date +
                ", type=" + type +
                '}';
    }

    public long getReference() {
        return reference;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPurpose() {
        return purpose;
    }

    public LocalDate getDate() {
        return date;
    }

    public TransactionType getType() {
        return type;
    }
}

class TransactionBuilder {
    private long reference;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String purpose;
    private LocalDate date;
    private TransactionType type;

    public TransactionBuilder setReference(long reference) {
        this.reference = reference;
        return this;
    }

    public TransactionBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public TransactionBuilder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public TransactionBuilder setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public TransactionBuilder setPurpose(String purpose) {
        this.purpose = purpose;
        return this;
    }

    public TransactionBuilder setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public TransactionBuilder setType(TransactionType type) {
        this.type = type;
        return this;
    }

    public Transaction createTransaction() {
        return new Transaction(reference, description, amount, currency, purpose, date, type);
    }
}
