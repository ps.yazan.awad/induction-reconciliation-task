package com.progressoft.jip9.finance;

import com.progressoft.jip9.finance.exceptions.CSVfileReaderException;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

public class CSVfileReader implements TransactionsReader {
    @Override
    public void read(Path path, Consumer<Transaction> consumer) {
        validatePath(path);
        try (BufferedReader reader = Files.newBufferedReader(path)){
            String line = reader.readLine();
            if (line == null)
                return;
            while ((line = reader.readLine()) != null){
                parseLine(consumer, line);
            }
        } catch (IOException e) {
            throw new CSVfileReaderException(e);
        }
    }

    private void parseLine(Consumer<Transaction> consumer, String line) {
        String[] fields = line.split(",", -1);
        long id =Long.parseLong(fields[0].split("-")[1]);
        TransactionType type=TransactionType.CREDIT;
        if(fields[6].equals("D"))
            type=TransactionType.DEBIT;
        LocalDate date = LocalDate.parse(fields[5], DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Transaction record = extractTransaction(fields, id, type, date);
        consumer.accept(record);
    }

    private Transaction extractTransaction(String[] fields, long id, TransactionType type, LocalDate date) {
        return new TransactionBuilder()
                .setReference(id)
                .setDescription(fields[1])
                .setAmount(new BigDecimal(fields[2]))
                .setCurrency(fields[3])
                .setDate(date)
                .setPurpose(fields[4])
                .setType(type)
                .createTransaction();
    }
}
