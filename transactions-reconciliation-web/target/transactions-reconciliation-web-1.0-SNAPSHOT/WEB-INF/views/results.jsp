<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>
<div class="container h-100 justify-content-center">
    <div class="jumbotron my-auto">
        <b>Logged In As : ${sessionScope.user.email}</b>
        <a style="margin-left: 30px" type="button" class="btn btn-danger"
           href="${pageContext.request.contextPath}/logout">Logout</a>
        <h1 style="text-align: center;margin-top: 50px">Reconciliation Results</h1>
        <div style="alignment: center;margin-top: 50px">
            <c:if test="${requestScope.resultsHolder ne null}">
                <section id="tabs" class="project-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <nav>
                                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                           href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"
                                           onclick="matchingTabOnClick();">Matching Transactions</a>
                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                           href="#nav-profile" role="tab" aria-controls="nav-profile"
                                           aria-selected="false"
                                           onclick="mismatchingTabOnClick();">Mismatching Transactions</a>
                                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                           href="#nav-contact" role="tab" aria-controls="nav-contact"
                                           aria-selected="false" onclick="missingTabOnClick();">Missing Transactions</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <table class="table" cellspacing="0" id="matching_table">
                                            <thead>
                                            <tr>
                                                <th>Transaction ID</th>
                                                <th>Amount</th>
                                                <th>Currency</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${requestScope.resultsHolder.matchingTransactions}"
                                                       var="transaction">
                                                <tr>
                                                    <td>${transaction.reference}</td>
                                                    <td>${transaction.amount}</td>
                                                    <td>${transaction.currency}</td>
                                                    <td>${transaction.date}</td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                         aria-labelledby="nav-profile-tab">
                                        <table class="table" cellspacing="0" id="mismatching_table">
                                            <thead>
                                            <tr>
                                                <th>Found In File</th>
                                                <th>Transaction ID</th>
                                                <th>Amount</th>
                                                <th>Currency</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${requestScope.resultsHolder.mismatchingTransactions}"
                                                       var="transaction">
                                                <tr>
                                                    <td>${transaction.sourceFile}</td>
                                                    <td>${transaction.transaction.reference}</td>
                                                    <td>${transaction.transaction.amount}</td>
                                                    <td>${transaction.transaction.currency}</td>
                                                    <td>${transaction.transaction.date}</td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                         aria-labelledby="nav-contact-tab">
                                        <table class="table" cellspacing="0" id="missing_table">
                                            <thead>
                                            <tr>
                                                <th>Found In File</th>
                                                <th>Transaction ID</th>
                                                <th>Amount</th>
                                                <th>Currency</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${requestScope.resultsHolder.missingTransactions}"
                                                       var="transaction">
                                                <tr>
                                                    <td>${transaction.sourceFile}</td>
                                                    <td>${transaction.transaction.reference}</td>
                                                    <td>${transaction.transaction.amount}</td>
                                                    <td>${transaction.transaction.currency}</td>
                                                    <td>${transaction.transaction.date}</td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div style="display: inline-grid" class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Download File
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" id="save_csv"
                           onclick="exportTableToCSV('MatchingTransactions.csv','matching_table')">Download as CSV</a>
                        <a class="dropdown-item" id="save_json"
                           onclick="exportTableToJSON('MatchingTransactions.json','#matching_table')">Download as Json</a>
                    </div>
                </div>
            </c:if>
            <a style="display: inline-grid" class="btn btn-primary" href="${pageContext.request.contextPath}/activity" role="button">Recent Activity</a>
            <a style="display: inline-grid" class="btn btn-primary" href="${pageContext.request.contextPath}/Reconciliation" role="button">Back to Reconciliation</a>
        </div>
    </div>
</div>
<script>
    function downloadCSV(csv, filename) {
        var csvFile;
        var downloadLink;

        // CSV file
        csvFile = new Blob([csv], {type: "text/csv"});

        // Download link
        downloadLink = document.createElement("a");

        // File name
        downloadLink.download = filename;

        // Create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Hide download link
        downloadLink.style.display = "none";

        // Add the link to DOM
        document.body.appendChild(downloadLink);

        // Click download link
        downloadLink.click();
    }
    function downloadJSON(json, filename) {
        var jsonFile;
        var downloadLink;

        jsonFile = new Blob([json], {type: "text/json"});

        downloadLink = document.createElement("a");

        downloadLink.download = filename;

        downloadLink.href = window.URL.createObjectURL(jsonFile);

        downloadLink.style.display = "none";

        document.body.appendChild(downloadLink);

        downloadLink.click();
    }
    function exportTableToCSV(filename, table) {
        var csv = [];
        var rows = document.getElementById(table).querySelectorAll("table tr");

        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++)
                row.push(cols[j].innerText);

            csv.push(row.join(","));
        }

        // Download CSV file
        downloadCSV(csv.join("\n"), filename);
    }

    function exportTableToJSON(filename, table) {
        var head = [],
            i = 0,
            tableObj = {transaction: []};
        $.each($(table+" thead th"), function() {
            head[i++] = $(this).text();
        });

        $.each($(table+" tbody tr"), function() {
            var $row = $(this),
                rowObj = {};

            i = 0;
            $.each($("td", $row), function() {
                var $col = $(this);
                rowObj[head[i]] = $col.text();
                i++;
            })

            tableObj.transaction.push(rowObj);
        });

        downloadJSON(JSON.stringify(tableObj),filename);
    }

    function matchingTabOnClick() {
        document.getElementById('save_csv').setAttribute('onclick', "exportTableToCSV('MatchingTransactions.csv','matching_table')");
        document.getElementById('save_json').setAttribute('onclick', "exportTableToJSON('MatchingTransactions.json','#matching_table')");
    }

    function mismatchingTabOnClick() {
        document.getElementById('save_csv').setAttribute('onclick', "exportTableToCSV('MismatchingTransactions.csv','mismatching_table')");
        document.getElementById('save_json').setAttribute('onclick', "exportTableToJSON('MismatchingTransactions.json','#mismatching_table')");
    }

    function missingTabOnClick() {
        document.getElementById('save_csv').setAttribute('onclick', "exportTableToCSV('MissingTransactions.csv','missing_table')");
        document.getElementById('save_json').setAttribute('onclick', "exportTableToJSON('MissingTransactions.json','#missing_table')");
    }
</script>
<style>
    .project-tab {
        padding: 10%;
        margin-top: -8%;
    }

    .project-tab #tabs {
        background: #007b5e;
        color: #eee;
    }

    .project-tab #tabs h6.section-title {
        color: #eee;
    }

    .project-tab #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #0062cc;
        background-color: transparent;
        border-color: transparent transparent #f3f3f3;
        border-bottom: 3px solid !important;
        font-size: 16px;
        font-weight: bold;
    }

    .project-tab .nav-link {
        border: 1px solid transparent;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
        color: #0062cc;
        font-size: 16px;
        font-weight: 600;
    }

    .project-tab .nav-link:hover {
        border: none;
    }

    .project-tab thead {
        background: #f3f3f3;
        color: #333;
    }

    .project-tab a {
        text-decoration: none;
        color: #333;
        font-weight: 600;
    }
</style>