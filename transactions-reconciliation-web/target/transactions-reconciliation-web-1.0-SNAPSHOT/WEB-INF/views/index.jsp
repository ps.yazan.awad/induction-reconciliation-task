<body>
<div class="container h-100 d-flex justify-content-center">
    <div class="jumbotron my-auto">
        <h1>Welcome To Reconciliation System</h1>
        <div class="display-3" style="text-align: center"><a class="btn btn-primary" href="${pageContext.request.contextPath}/login" role="button" style="align-self: center">Click here to Login</a></div>
    </div>
</div>