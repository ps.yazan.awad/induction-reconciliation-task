<body>
<div class="container h-100 d-flex justify-content-center">
    <div class="jumbotron my-auto">
        <b>Logged In As : ${sessionScope.user.email}</b>
        <a style="margin-left: 30px" type="button" class="btn btn-danger" href="${pageContext.request.contextPath}/logout">Logout</a>
        <h1 style="text-align: center;margin-top: 30px">Ready To Compare</h1>
        <div class="container h-100 d-flex justify-content-center"style="margin-top: 100px">
            <div style="display: inline-grid;margin-right: 30px;width: 200px;height: 150px">
                <div style="background: dodgerblue">
                    <h3 style="color: white;text-align: center">Source</h3>
                </div>
                <div style="border-color:dodgerblue;border-style: solid;border-width: thin">
                    <label style="font-weight: bold" for="source_name">Name : </label> <span  id="source_name">File1<br></span>
                    <label style="font-weight: bold" for="source_type">Type : </label> <span  id="source_type">CSV</span>
                </div>
            </div>
            <div style="display: inline-grid;margin-left: 30px;width: 200px;height: 150px">
                <div style="background: dodgerblue">
                    <h3 style="color: white;text-align: center">Target</h3>
                </div>
                <div style="border-color:dodgerblue;border-style: solid;border-width: thin">
                    <label style="font-weight: bold" for="target_name">Name : </label> <span  id="target_name">File2<br></span>
                    <label style="font-weight: bold" for="target_type">Type : </label> <span  id="target_type">Json</span>
                </div>
            </div>
        </div>
        <form>
            <div style="text-align: center;margin-top: 50px">
                <a href="${pageContext.request.contextPath}/sourceUpload" role="button" class="btn btn-danger"style="margin-right: 200px">Cancel</a>
                <a href="${pageContext.request.contextPath}/targetUpload" role="button" class="btn btn-primary">Back</a>
                <button type="submit" class="btn btn-success">Compare</button>
            </div>
        </form>
    </div>
</div>