<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>
<div class="container h-100 justify-content-center">
    <div class="jumbotron my-auto">
        <b>Logged In As : ${sessionScope.user.email}</b>
        <a style="margin-left: 30px" type="button" class="btn btn-danger"
           href="${pageContext.request.contextPath}/logout">Logout</a>
        <h1 style="text-align: center;margin-top: 50px">Your Recent Activity</h1>
        <section style="margin-top: 50px">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Session Time</th>
                    <th scope="col">Reconciliation Time</th>
                    <th scope="col">Source Name</th>
                    <th scope="col">Target Name</th>
                    <th scope="col">Matched</th>
                    <th scope="col">Mismatched</th>
                    <th scope="col">Missing</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestScope.recentActivityList}" var="activity">
                    <tr>
                        <td>${activity.sessionTime}</td>
                        <td>${activity.reconciliationTime}</td>
                        <td>${activity.sourceName}</td>
                        <td>${activity.targetName}</td>
                        <td>${activity.matchedCount}</td>
                        <td>${activity.mismatchedCount}</td>
                        <td>${activity.missingCount}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </section>
        <a style="display: inline-grid" class="btn btn-primary" href="${pageContext.request.contextPath}/Reconciliation" role="button">Back to Reconciliation</a>
    </div>
</div>
<script>
    var tables = document.getElementsByTagName('table');
    var table = tables[tables.length - 1];
    var rows = table.rows;
    for(var i = 0, td; i < rows.length; i++){
        td = document.createElement('td');
        td.appendChild(document.createTextNode(i + 1));
        rows[i+1].insertBefore(td, rows[i+1].firstChild);
    }
</script>