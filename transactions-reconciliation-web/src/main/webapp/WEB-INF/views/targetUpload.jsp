<body>
<div class="container h-100 d-flex justify-content-center">
    <div class="jumbotron my-auto">
        <b>Logged In As : ${sessionScope.user.email}</b>
        <a style="margin-left: 30px" type="button" class="btn btn-danger" href="${pageContext.request.contextPath}/logout">Logout</a>
        <h1 style="margin-top: 30px">Target File Upload</h1>
        <form method="post" enctype="multipart/form-data">
            <div style="margin-top: 50px">
                <label for="trgt_name">Target name:</label>
                <input id="trgt_name" type="text" class="form-control" name="targetName">
            </div>
            <label for="inlineFormCustomSelect" style="margin-top: 20px">File Type:</label>
            <div class="form-row align-items-center">
                <div class="col-auto my-1">
                    <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="fileType">
                        <option value="csv">Csv</option>
                        <option value="json">Json</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="margin-top: 20px">
                <label for="target_file_upload">Target file:</label>
                <input type="file" class="form-control-file" id="target_file_upload" value="Upload" name="target_file_upload">
            </div>
            <div style="text-align: center;margin-top: 20px">
                <a href="${pageContext.request.contextPath}/sourceUpload" role="button" class="btn btn-primary">Previous</a>
                <button type="submit" class="btn btn-primary">Next</button></div>
        </form>
    </div>
</div>