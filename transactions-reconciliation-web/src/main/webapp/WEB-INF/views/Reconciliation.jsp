<body onload="showSourcePart();">
<div class="container h-100 d-flex justify-content-center">
    <div class="jumbotron my-auto">
        <b>Logged In As : ${sessionScope.user.email}</b>
        <a style="margin-left: 30px" type="button" class="btn btn-danger"
           href="${pageContext.request.contextPath}/logout">Logout</a>
        <h1 style="margin-top: 30px;text-align: center" id="heading_line"></h1>
        <form method="POST" action="Reconciliation" enctype="multipart/form-data">
            <div id="source_upload_div">
                <div style="margin-top: 50px">
                    <label for="src_name">Source name:</label>
                    <input id="src_name" type="text" class="form-control" name="SourceName" onchange="fillReadyToCompareData()">
                </div>
                <div style="margin-top: 20px"><label for="source_select_type">File type:</label></div>
                <div class="form-row align-items-center">
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="source_select_type">Preference</label>
                        <select class="custom-select mr-sm-2" id="source_select_type" name="source_select_type" onchange="fillReadyToCompareData()">
                            <option value="csv">Csv</option>
                            <option value="json">Json</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-top: 20px">
                    <label for="source_file_upload">Source file:</label>
                    <input type="file" class="form-control-file" id="source_file_upload" value="Upload"
                           name="source_file_upload">
                </div>
                <div style="text-align: center;margin-top: 20px">
                    <button type="button" onclick="showTargetPart()" class="btn btn-primary">Next</button>
                </div>
            </div>
            <%--------------%>
            <div id="target_upload_div">
                <div style="margin-top: 50px">
                    <label for="trgt_name">Target name:</label>
                    <input id="trgt_name" type="text" class="form-control" name="targetName" onchange="fillReadyToCompareData()">
                </div>
                <label for="target_select_type" style="margin-top: 20px">File Type:</label>
                <div class="form-row align-items-center">
                    <div class="col-auto my-1">
                        <label class="mr-sm-2 sr-only" for="target_select_type">Preference</label>
                        <select class="custom-select mr-sm-2" id="target_select_type" name="target_select_type" onchange="fillReadyToCompareData()">
                            <option value="csv">Csv</option>
                            <option value="json">Json</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-top: 20px">
                    <label for="target_file_upload">Target file:</label>
                    <input type="file" class="form-control-file" id="target_file_upload" value="Upload"
                           name="target_file_upload">
                </div>
                <div style="text-align: center;margin-top: 20px">
                    <button type="button" onclick="showSourcePart()" class="btn btn-primary">Previous</button>
                    <button type="button" onclick="showReadyToComparePart()" class="btn btn-primary">Next</button>
                </div>
            </div>
            <%-----------------------%>
            <div id="readyToCompare_div">
                <div class="container h-100 d-flex justify-content-center" style="margin-top: 100px">
                    <div style="display: inline-grid;margin-right: 30px;width: 200px;height: 150px">
                        <div style="background: dodgerblue">
                            <h3 style="color: white;text-align: center">Source</h3>
                        </div>
                        <div style="border-color:dodgerblue;border-style: solid;border-width: thin">
                            <label style="font-weight: bold" for="source_name">Name : </label>
                            <span id="source_name"></span><br>
                            <label style="font-weight: bold" for="source_type">Type : </label>
                            <span id="source_type"></span><br>
                        </div>
                    </div>
                    <div style="display: inline-grid;margin-left: 30px;width: 200px;height: 150px">
                        <div style="background: dodgerblue">
                            <h3 style="color: white;text-align: center">Target</h3>
                        </div>
                        <div style="border-color:dodgerblue;border-style: solid;border-width: thin">
                            <label style="font-weight: bold" for="target_name">Name : </label>
                            <span id="target_name"></span><br>
                            <label style="font-weight: bold" for="target_type">Type : </label>
                            <span id="target_type"></span><br>
                        </div>
                    </div>
                </div>
                <div style="text-align: center;margin-top: 50px">
                    <button type="button" onclick="cancelButtonEvent()" class="btn btn-danger" style="margin-right: 200px">Cancel</button>
                    <button type="button" onclick="showTargetPart()" class="btn btn-primary">Back</button>
                    <button type="submit" class="btn btn-success">Compare</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function showTargetPart() {
        $("#target_upload_div").show();
        $("#source_upload_div").hide();
        $("#readyToCompare_div").hide();
        $("#heading_line").text("Target File Upload");
    }

    function showSourcePart() {
        $("#source_upload_div").show();
        $("#readyToCompare_div").hide();
        $("#target_upload_div").hide();
        $("#heading_line").text("Source File Upload");
    }

    function showReadyToComparePart() {
        $("#readyToCompare_div").show();
        $("#target_upload_div").hide();
        $("#source_upload_div").hide();
        $("#heading_line").text("Ready To Compare");
    }
    function cancelButtonEvent() {
        $("#src_name").val('');
        $("#source_file_upload").val('');
        $("#trgt_name").val('');
        $("#target_file_upload").val('');
        showSourcePart();
    }
    function fillReadyToCompareData() {
        $("#source_name").text($("#src_name").val());
        $("#target_name").text($("#trgt_name").val());
        $("#source_type").text($("#source_select_type").val());
        $("#target_type").text($("#target_select_type").val());
    }

</script>