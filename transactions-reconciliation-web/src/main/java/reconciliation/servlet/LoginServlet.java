package reconciliation.servlet;

import reconciliation.model.User;
import reconciliation.model.UserDAO;
import reconciliation.model.exceptions.UserAuthenticationException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private final UserDAO userDAO;

    public LoginServlet(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        try {
            String destPage = validateUser(req, email, password);
            // TODO a redirect for successful login would be better
            RequestDispatcher dispatcher = req.getRequestDispatcher(destPage);
            dispatcher.forward(req, resp);
        } catch (UserAuthenticationException e) {
            String message = "Authentication Failed !";
            req.setAttribute("message", message);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
            dispatcher.forward(req, resp);
        }
    }

    private String validateUser(HttpServletRequest req, String email, String password) {
        User user = userDAO.checkLogin(email, password);
        String destPage = "/WEB-INF/views/login.jsp";

        if (user != null) {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            destPage = "/WEB-INF/views/Reconciliation.jsp";
        } else {
            String message = "Invalid email/password";
            req.setAttribute("message", message);
        }
        return destPage;
    }

    public void destroy()
    {
        userDAO.closeConnection();
    }
}
