package reconciliation.servlet;

import com.progressoft.jip9.finance.CSVfileReader;
import com.progressoft.jip9.finance.JsonFileReader;
import com.progressoft.jip9.finance.ReconciliationHandler;
import com.progressoft.jip9.finance.TransactionsReader;
import com.progressoft.jip9.finance.comparison.ResultsHolder;
import reconciliation.model.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

@MultipartConfig(location = "/tmp", fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class ReconciliationServlet extends HttpServlet {
    private ActivityDAO activityDAO;

    public ReconciliationServlet(ActivityDAO activityDAO) {
        this.activityDAO = activityDAO;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/Reconciliation.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sourceName = req.getParameter("SourceName");
        String targetName = req.getParameter("targetName");
        String sourceType = req.getParameter("source_select_type");
        String targetType = req.getParameter("target_select_type");
        ResultsHolder resultsHolder = getResultsHolder(req, sourceType, targetType);
        setSessionAttributes(req, sourceName, targetName, resultsHolder);
        addRecentActivity(req, sourceName, targetName, resultsHolder);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/views/results.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void setSessionAttributes(HttpServletRequest req, String sourceName, String targetName, ResultsHolder resultsHolder) {
        req.setAttribute("resultsHolder", resultsHolder);
        req.setAttribute("SourceName", sourceName);
        req.setAttribute("targetName", targetName);
    }

    private void addRecentActivity(HttpServletRequest req, String sourceName, String targetName, ResultsHolder resultsHolder) {
        HttpSession session = req.getSession();
        Date creationTime = new Date(session.getCreationTime());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;

        RecentActivity recentActivity = new RecentActivityBuilder()
                .setSessionTime(format.format(creationTime))
                .setSourceName(sourceName)
                .setTargetName(targetName)
                .setUser((User) session.getAttribute("user"))
                .setReconciliationTime(format.format(new Date()))
                .setMatchedCount(resultsHolder.getMatchingTransactions().size())
                .setMismatchedCount(resultsHolder.getMismatchingTransactions().size())
                .setMissingCount(resultsHolder.getMissingTransactions().size())
                .createRecentActivity();

        activityDAO.addActivity(recentActivity);
    }

    private ResultsHolder getResultsHolder(HttpServletRequest req, String sourceType, String targetType) throws IOException, ServletException {
        Path sourceTempFile = getTempFile(req, "source_file_upload", sourceType);
        Path targetTempFile = getTempFile(req, "target_file_upload", targetType);
        TransactionsReader sourceReader = null;
        TransactionsReader targetReader = null;
        sourceReader = getSourceReader(sourceType, sourceReader);
        targetReader = getTargetReader(targetType, targetReader);

        ReconciliationHandler reconciliationHandler = new ReconciliationHandler();
        ResultsHolder resultsHolder = reconciliationHandler.handle(sourceReader, targetReader, sourceTempFile, targetTempFile);
        return resultsHolder;
    }

    private TransactionsReader getTargetReader(String targetType, TransactionsReader targetReader) {
        if (targetType.equalsIgnoreCase("csv"))
            targetReader = new CSVfileReader();
        else if (targetType.equalsIgnoreCase("json"))
            targetReader = new JsonFileReader();
        return targetReader;
    }

    private TransactionsReader getSourceReader(String sourceType, TransactionsReader sourceReader) {
        if (sourceType.equalsIgnoreCase("csv"))
            sourceReader = new CSVfileReader();
        else if (sourceType.equalsIgnoreCase("json"))
            sourceReader = new JsonFileReader();
        return sourceReader;
    }

    private Path getTempFile(HttpServletRequest req, String part, String fileType) throws IOException, ServletException {
        Part filePart = req.getPart(part);
        InputStream fileInputStream = filePart.getInputStream();
        char charArray[] = new char[fileInputStream.available()];
        new InputStreamReader(fileInputStream).read(charArray);
        String contents = new String(charArray);
        Path tempFile = Files.createTempFile("ReconciliationFile-" + filePart.getSubmittedFileName(), "." + fileType);
        Files.write(tempFile, contents.getBytes());
        return tempFile;
    }
    public void destroy()
    {
        activityDAO.closeConnection();
    }
}
