package reconciliation.servlet;

import reconciliation.model.ActivityDAO;
import reconciliation.model.RecentActivity;
import reconciliation.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ActivityServlet extends HttpServlet {
    ActivityDAO activityDAO;

    public ActivityServlet(ActivityDAO activityDAO) {
        this.activityDAO = activityDAO;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<RecentActivity> recentActivityList = activityDAO.getRecentActivities((User) session.getAttribute("user"));
        req.setAttribute("recentActivityList",recentActivityList);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/recentActivity.jsp");
        requestDispatcher.forward(req,resp);
    }
}
