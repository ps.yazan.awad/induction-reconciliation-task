package reconciliation.model;


public class RecentActivityBuilder {
    private String sessionTime;
    private User user;
    private String reconciliationTime;
    private String sourceName;
    private String targetName;
    private int matchedCount;
    private int mismatchedCount;
    private int missingCount;

    public RecentActivityBuilder setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
        return this;
    }

    public RecentActivityBuilder setUser(User user) {
        this.user = user;
        return this;
    }

    public RecentActivityBuilder setReconciliationTime(String reconciliationTime) {
        this.reconciliationTime = reconciliationTime;
        return this;
    }

    public RecentActivityBuilder setSourceName(String sourceName) {
        this.sourceName = sourceName;
        return this;
    }

    public RecentActivityBuilder setTargetName(String targetName) {
        this.targetName = targetName;
        return this;
    }

    public RecentActivityBuilder setMatchedCount(int matchedCount) {
        this.matchedCount = matchedCount;
        return this;
    }

    public RecentActivityBuilder setMismatchedCount(int mismatchedCount) {
        this.mismatchedCount = mismatchedCount;
        return this;
    }

    public RecentActivityBuilder setMissingCount(int missingCount) {
        this.missingCount = missingCount;
        return this;
    }

    public RecentActivity createRecentActivity() {
        return new RecentActivity(sessionTime, user, reconciliationTime, sourceName, targetName, matchedCount, mismatchedCount, missingCount);
    }
}