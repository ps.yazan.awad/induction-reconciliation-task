package reconciliation.model;

import java.sql.SQLException;
import java.util.List;

public interface ActivityDAO {
    public void addActivity(RecentActivity recentActivity);
    public List<RecentActivity> getRecentActivities(User user);
    public void closeConnection();
}
