package reconciliation.model;

import reconciliation.model.exceptions.DbConnectionException;
import reconciliation.model.exceptions.UserAuthenticationException;

import java.sql.*;

public class UserDAOMySQL implements UserDAO{
    DbConnection dbConnection;

    public UserDAOMySQL(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public User checkLogin(String email, String password) throws UserAuthenticationException{
        try {
            Connection connection = dbConnection.getConnection();
            String sql = "SELECT * FROM users WHERE email = ? and password = ?";
            String hashPassword = byteArrayToHexString(computeHash(password));
            // TODO statements are not closed
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, email);
            statement.setString(2, hashPassword);
            return getUser(email, statement);
        }catch (SQLException e){
            throw new UserAuthenticationException("Authentication Failed !",e);
        }catch (Exception e) {
            throw new UserAuthenticationException("Authentication Failed !",e);
        }
    }

    @Override
    public void closeConnection() {
        try {
            dbConnection.getConnection().close();
        } catch (SQLException e) {
            throw new DbConnectionException("Closing connection Failed !",e);
        }
    }

    private User getUser(String email, PreparedStatement statement) throws SQLException {
        try(ResultSet result = statement.executeQuery();) {
            User user = null;
            if (result.next()) {
                user = new User();
                user.setId(Integer.parseInt(result.getString("id")));
                user.setEmail(email);
            }
            return user;
        }
    }
    public static byte[] computeHash(String x)
            throws Exception
    {
        java.security.MessageDigest d =null;
        d = java.security.MessageDigest.getInstance("SHA-1");
        d.reset();
        d.update(x.getBytes());
        return  d.digest();
    }

    public static String byteArrayToHexString(byte[] b){
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++){
            int v = b[i] & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
    }
}

