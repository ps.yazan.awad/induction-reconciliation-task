package reconciliation.model.exceptions;

public class RecentActivityException extends RuntimeException{
    public RecentActivityException(String message, Exception cause) {
        super(message, cause);
    }
}
