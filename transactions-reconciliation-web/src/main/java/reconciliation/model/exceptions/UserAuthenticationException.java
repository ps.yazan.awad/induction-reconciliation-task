package reconciliation.model.exceptions;

public class UserAuthenticationException extends RuntimeException{
    public UserAuthenticationException(String message, Exception e) {
        super(message, e);
    }
}
