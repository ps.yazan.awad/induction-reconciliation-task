package reconciliation.model.exceptions;

public class DbConnectionException extends RuntimeException{
    public DbConnectionException(String message, Exception cause) {
        super(message, cause);
    }
}
