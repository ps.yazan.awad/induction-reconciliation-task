package reconciliation.model;


import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

public class RecentActivity {
    private String sessionTime;
    private User user;
    private String reconciliationTime;
    private String sourceName;
    private String targetName;
    private int matchedCount;
    private int mismatchedCount;
    private int missingCount;

    public RecentActivity(String sessionTime, User user, String reconciliationTime, String sourceName, String targetName, int matchedCount, int mismatchedCount, int missingCount) {
        this.sessionTime = sessionTime;
        this.user = user;
        this.reconciliationTime = reconciliationTime;
        this.sourceName = sourceName;
        this.targetName = targetName;
        this.matchedCount = matchedCount;
        this.mismatchedCount = mismatchedCount;
        this.missingCount = missingCount;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public User getUser() {
        return user;
    }

    public String getReconciliationTime() {
        return reconciliationTime;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getTargetName() {
        return targetName;
    }

    public int getMatchedCount() {
        return matchedCount;
    }

    public int getMismatchedCount() {
        return mismatchedCount;
    }

    public int getMissingCount() {
        return missingCount;
    }

    @Override
    public String toString() {
        return "RecentActivity{" +
                "sessionTime='" + sessionTime + '\'' +
                ", user=" + user +
                ", reconciliationTime='" + reconciliationTime + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", targetName='" + targetName + '\'' +
                ", matchedCount=" + matchedCount +
                ", mismatchedCount=" + mismatchedCount +
                ", missingCount=" + missingCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecentActivity that = (RecentActivity) o;
        return matchedCount == that.matchedCount &&
                mismatchedCount == that.mismatchedCount &&
                missingCount == that.missingCount &&
                (sessionTime.equals(sessionTime)) &&
                Objects.equals(user, that.user) &&
                (reconciliationTime.equals(reconciliationTime)) &&
                Objects.equals(sourceName, that.sourceName) &&
                Objects.equals(targetName, that.targetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionTime, user, reconciliationTime, sourceName, targetName, matchedCount, mismatchedCount, missingCount);
    }
}
