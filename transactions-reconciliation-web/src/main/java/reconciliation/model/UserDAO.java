package reconciliation.model;

import reconciliation.model.exceptions.UserAuthenticationException;


public interface UserDAO {
     User checkLogin(String email, String password) throws UserAuthenticationException;
     public void closeConnection();
}
