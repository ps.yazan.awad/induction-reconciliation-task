package reconciliation.model;

import reconciliation.model.exceptions.DbConnectionException;
import reconciliation.model.exceptions.RecentActivityException;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ActivityDAOMysql implements ActivityDAO {
    DbConnection dbConnection;

    public ActivityDAOMysql(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public void addActivity(RecentActivity recentActivity) {
        try {
            String sql = "INSERT INTO RecentActivity " +
                    "(user,ssn_time,reconciliation_time,source_name,target_name,matched_count,mismatched_count,missing_count) " +
                    "values (?,?,?,?,?,?,?,?)";
            // TODO this is a bad practice for getting a connection, use DataSource instead then you can control the single connection from it
            PreparedStatement statement = dbConnection.getConnection().prepareStatement(sql);
            prepareAddStatement(recentActivity, statement);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RecentActivityException("Adding Activity Failed !", e);
        }
    }

    private void prepareAddStatement(RecentActivity recentActivity, PreparedStatement statement) throws SQLException {
        statement.setString(1, String.valueOf(recentActivity.getUser().getId()));
        statement.setString(2, String.valueOf(recentActivity.getSessionTime()));
        statement.setString(3, String.valueOf(recentActivity.getReconciliationTime()));
        statement.setString(4, recentActivity.getSourceName());
        statement.setString(5, recentActivity.getTargetName());
        statement.setString(6, String.valueOf(recentActivity.getMatchedCount()));
        statement.setString(7, String.valueOf(recentActivity.getMismatchedCount()));
        statement.setString(8, String.valueOf(recentActivity.getMissingCount()));
    }

    @Override
    public List<RecentActivity> getRecentActivities(User user) {
        try {
            Connection connection = dbConnection.getConnection();
            String sql = "SELECT * FROM RecentActivity WHERE user = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, String.valueOf(user.getId()));
            try (ResultSet result = statement.executeQuery();) {
                List<RecentActivity> recentActivityList = new LinkedList<>();
                while (result.next()) {
                    fillRecentActivityList(user, result, recentActivityList);
                }
                return recentActivityList;
            }
        } catch (SQLException e) {
            throw new RecentActivityException("Getting Recent Activity Failed!", e);
        }
    }

    private void fillRecentActivityList(User user, ResultSet result, List<RecentActivity> recentActivityList) throws SQLException {
        RecentActivity recentActivity = new RecentActivityBuilder()
                .setUser(user)
                .setSessionTime(result.getString("ssn_time"))
                .setReconciliationTime(result.getString("reconciliation_time"))
                .setSourceName(result.getString("source_name"))
                .setTargetName(result.getString("target_name"))
                .setMatchedCount(result.getInt("matched_count"))
                .setMismatchedCount(result.getInt("mismatched_count"))
                .setMissingCount(result.getInt("missing_count"))
                .createRecentActivity();
        recentActivityList.add(recentActivity);
    }

    @Override
    public void closeConnection() {
        try {
            dbConnection.getConnection().close();
        } catch (SQLException e) {
            throw new DbConnectionException("Closing connection Failed!", e);
        }
    }
}
