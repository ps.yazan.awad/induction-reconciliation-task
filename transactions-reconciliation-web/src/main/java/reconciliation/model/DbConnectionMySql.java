package reconciliation.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnectionMySql implements DbConnection {
    // TODO why not using DataSource implementation, now all application would run in a single connection
    Connection connection;

    public DbConnectionMySql(String jdbcURL, String dbUser, String dbPassword) throws SQLException {
        connection = DriverManager.getConnection(jdbcURL, dbUser, dbPassword);
    }

    @Override
    public Connection getConnection(){
        return connection;
    }
}
