package reconciliation.initializer;

import reconciliation.filters.AuthenticationFilter;
import reconciliation.model.*;
import reconciliation.model.exceptions.DbConnectionException;
import reconciliation.servlet.*;

import javax.servlet.*;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.Set;

public class WebInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        DbConnection dbConnection = createConnection(servletContext);
        ActivityDAO activityDAO = new ActivityDAOMysql(dbConnection);
        UserDAO userDAO = new UserDAOMySQL(dbConnection);
        registerLoginServlet(servletContext,userDAO);
        registerReconciliationServletServlet(servletContext,activityDAO);
        registerLogoutServlet(servletContext);
        registerActivityServlet(servletContext,activityDAO);

        registerAuthenticationFilter(servletContext);
    }

    private DbConnection createConnection(ServletContext servletContext) {
        DbConnection dbConnection = null;
        String jdbcUrl = servletContext.getInitParameter("jdbc-url");
        String user = servletContext.getInitParameter("jdbc-user");
        String pass = servletContext.getInitParameter("jdbc-pass");
        try {
             dbConnection = new DbConnectionMySql(jdbcUrl,user,pass);
        } catch (SQLException e) {
            throw new DbConnectionException("Couldn't connect to DB !", e);
        }
        return dbConnection;
    }

    private void registerActivityServlet(ServletContext servletContext, ActivityDAO activityDAO) {
        ActivityServlet activityServlet = new ActivityServlet(activityDAO);
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("activityServlet", activityServlet);
        servletRegistration.addMapping("/activity");
    }

    private void registerAuthenticationFilter(ServletContext servletContext) {
        AuthenticationFilter authenticationFilter = new AuthenticationFilter();
        FilterRegistration.Dynamic filterRegistration = servletContext.addFilter("authenticationFilter", authenticationFilter);
        filterRegistration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
    }

    private void registerLogoutServlet(ServletContext servletContext) {
        LogoutServlet logoutServlet = new LogoutServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("logoutServlet", logoutServlet);
        servletRegistration.addMapping("/logout");
    }

    private void registerReconciliationServletServlet(ServletContext servletContext,ActivityDAO activityDAO) {
        ReconciliationServlet reconciliationServlet = new ReconciliationServlet(activityDAO);
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("reconciliationServlet", reconciliationServlet);
        servletRegistration.setMultipartConfig(new MultipartConfigElement("data/tmp", 1048576, 1048576, 262144));
        servletRegistration.addMapping("/Reconciliation");
    }

    private void registerLoginServlet(ServletContext servletContext, UserDAO userDAO) {
        LoginServlet loginServlet = new LoginServlet(userDAO);
        ServletRegistration.Dynamic WelcomeServletRegistration = servletContext.addServlet("LoginServlet", loginServlet);
        WelcomeServletRegistration.addMapping("/login");
    }

}
