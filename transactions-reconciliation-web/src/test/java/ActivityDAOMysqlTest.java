import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
import reconciliation.model.*;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

public class ActivityDAOMysqlTest {
    DbConnection connection;
    PreparedStatement statement;

    @Before
    public void setUpDB() {

        try {
            String jdbcUrl = "jdbc:mysql://localhost:3306/Reconciliation?serverTimezone=UTC";
            connection = new DbConnectionMySql(jdbcUrl, "root", "");
            String sql = "INSERT INTO RecentActivity " +
                    "(user,ssn_time,reconciliation_time,source_name,target_name,matched_count,mismatched_count,missing_count) " +
                    "values (?,?,?,?,?,?,?,?)";
            statement = connection.getConnection().prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            fail("Error instantiating database table: " + e.getMessage());
        }
    }

    @Test
    public void testGetData() throws Exception {
        addData();

        ActivityDAOMysql dao = new ActivityDAOMysql(connection);
        User user = new User();
        user.setId(1);
        user.setEmail("yazan@progressoft.com");
        List<RecentActivity> recentActivityList = dao.getRecentActivities(user);
        RecentActivity expectedRecentActivity = new RecentActivityBuilder()
                .setSessionTime("2019-12-1 12:12:12")
                .setSourceName("UnitTest")
                .setTargetName("UnitTest")
                .setUser(user)
                .setReconciliationTime("2019-12-1 12:12:12")
                .setMatchedCount(100)
                .setMismatchedCount(100)
                .setMissingCount(100)
                .createRecentActivity();
        boolean found = false;
        for (RecentActivity recentActivity : recentActivityList) {
            if (recentActivity.equals(expectedRecentActivity))
                found = true;
        }
        Assertions.assertTrue(found, "Test Activity not Added");
    }

    @After
    public void tearDown() {
        try {
            connection.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addData() throws SQLException {
        statement.setString(1, String.valueOf(1));
        statement.setString(2, "2019-12-1 12:12:12");
        statement.setString(3, "2019-12-1 12:12:12");
        statement.setString(4, "UnitTest");
        statement.setString(5, "UnitTest");
        statement.setString(6, String.valueOf(100));
        statement.setString(7, String.valueOf(100));
        statement.setString(8, String.valueOf(100));
        statement.executeUpdate();
    }
}
