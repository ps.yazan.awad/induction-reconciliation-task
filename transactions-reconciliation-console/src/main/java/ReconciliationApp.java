import com.progressoft.jip9.finance.*;
import com.progressoft.jip9.finance.comparison.Comparison;
import com.progressoft.jip9.finance.comparison.ResultsHolder;
import com.progressoft.jip9.finance.comparison.TransactionComparison;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ReconciliationApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sourceLocationInput = getSourceLocationInput(scanner);
        String sourceFileFormat = getSourceFileFormat(scanner);
        String targetLocationInput = getTargetLocationInput(scanner);
        String targetFileFormat = getTargetFileFormat(scanner);
        Path sourcePath = Paths.get(sourceLocationInput);
        Path targetPath = Paths.get(targetLocationInput);
        TransactionsReader sourceReader = getSourceReader(sourceFileFormat);
        TransactionsReader targetReader = getTargetReader(targetFileFormat);

        //TODO you could enhance this by moving all of lines below under a class
        // name it ReconciliationHandler
        ReconciliationHandler handler = new ReconciliationHandler();
        ResultsHolder resultsHolder = handler.handle(sourceReader, targetReader, sourcePath, targetPath);
        ResultsFileWriter writer = new CsvFileWriter();
        writer.write(resultsHolder);
        System.out.println("Reconciliation finished.\n Result files are available in directory /reconciliation-results");
    }

    private static TransactionsReader getTargetReader(String targetFileFormat) {
        TransactionsReader targetReader;
        if (targetFileFormat.toUpperCase().equals("CSV"))
            targetReader = new CSVfileReader();
        else if (targetFileFormat.toUpperCase().equals("JSON"))
            targetReader = new JsonFileReader();
        else throw new IllegalStateException("File Format Error");
        return targetReader;
    }

    private static TransactionsReader getSourceReader(String sourceFileFormat) {
        TransactionsReader sourceReader;
        if (sourceFileFormat.toUpperCase().equals("CSV"))
            sourceReader = new CSVfileReader();
        else if (sourceFileFormat.toUpperCase().equals("JSON"))
            sourceReader = new JsonFileReader();
        else throw new IllegalStateException("File Format Error");
        return sourceReader;
    }

    private static String getTargetFileFormat(Scanner scanner) {
        System.out.println(">> Enter target file format:");
        String targetFileFormat = scanner.nextLine();
        while (!(targetFileFormat.toUpperCase().equals("CSV") || targetFileFormat.toUpperCase().equals("JSON"))) {
            System.out.println("Please enter a valid format (CSV , JSON): ");
            targetFileFormat = scanner.nextLine();
        }
        return targetFileFormat;
    }

    private static String getTargetLocationInput(Scanner scanner) {
        System.out.println(">> Enter target file location: ");
        return scanner.nextLine();
    }

    private static String getSourceFileFormat(Scanner scanner) {
        System.out.println(">> Enter source file format: ");
        String sourceFileFormat = scanner.nextLine();
        while (!(sourceFileFormat.toUpperCase().equals("CSV") || sourceFileFormat.toUpperCase().equals("JSON"))) {
            System.out.println("Please enter a valid format (CSV , JSON): ");
            sourceFileFormat = scanner.nextLine();
        }
        return sourceFileFormat;
    }

    private static String getSourceLocationInput(Scanner scanner) {
        System.out.println(">> Enter source file location: ");
        return scanner.nextLine();
    }
}
